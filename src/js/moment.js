'use strict'
import moment from 'moment'
var temp = {}
temp.install = function (Vue, option) {
  Vue.prototype.$moment = moment
}
export default temp
