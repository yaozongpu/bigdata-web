var temp = {}
temp.install = function (Vue, option) {
  Vue.prototype.$permission = {
    hasPermission: function (code) {
      var ps = sessionStorage.getItem('permission')
      if (ps) {
        var permissions = ps.split(',')
        for (var i = 0; i < permissions.length; i++) {
          if (permissions[i] === code) {
            return true
          }
        }
      }
      return false
    },
    hasPermissions: function (codes) {
      for (var i = 0; i < codes.length; i++) {
        if (this.hasPermission(codes[i])) {
          return true
        }
      }
      return false
    }
  }
}
export default temp
