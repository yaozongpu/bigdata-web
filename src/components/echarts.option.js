const color = ['#C0392B', '#2874A6', '#1ABC9C', '#F1C40F', '#27AE60', '#D35400', '#BDC3C7', '#2E4053', '#2ECC71', '#F39C12']
const dataCatagrayPie = {
  backgroundColor: '#f8f8f9',
  color: color,
  title: {
    text: '数据分类',
    top: 10,
    left: 10,
    textStyle: {
      color: '#17233d'
    }
  },
  grid: {
    width: '100%',
    height: 300
  },
  tooltip: {
    trigger: 'item',
    formatter: '{b} : {c}'
  },

//  visualMap: {
//    show: false,
//    min: 5000000,
//    max: 3000000000,
//    inRange: {
//      colorLightness: [0, 1]
//    }
//  },
  series: [
    {
      name: '数据分类统计',
      type: 'pie',
      radius: '65%',
      center: ['50%', '50%'],
      data: [],
      // roseType: 'radius',
      label: {
        normal: {
          textStyle: {
            color: '#515a6e'
          }
        }
      },
      labelLine: {
        normal: {
          lineStyle: {
            color: '#808695'
          },
          smooth: 0.2,
          length: 10,
          length2: 20
        }
      },
//      itemStyle: {
//        normal: {
//          color: '#c23531',
//          // shadowBlur: 200,
//          shadowColor: 'rgba(0, 0, 0, 0.5)'
//        }
//      },
      animationType: 'scale',
      animationEasing: 'elasticOut',
      animationDelay: function (idx) {
        return Math.random() * 200
      }
    }
  ]
}
const warningCatagrayPie = {
  backgroundColor: '#f8f8f9',
  color: color,
  title: {
    text: '重点人员分类',
    top: 10,
    left: 10,
    textStyle: {
      color: '#17233d'
    }
  },
  grid: {
    width: '100%',
    height: 300
  },
  tooltip: {
    trigger: 'item',
    formatter: '{b} : {c}'
  },
  series: [
    {
      name: '数据分类统计',
      type: 'pie',
      radius: '65%',
      center: ['50%', '50%'],
      data: [
      ].sort(function (a, b) { return a.value - b.value }),
      // roseType: 'radius',
      label: {
        normal: {
          textStyle: {
            color: '#515a6e'
          }
        }
      },
      labelLine: {
        normal: {
          lineStyle: {
            color: '#808695'
          },
          smooth: 0.2,
          length: 10,
          length2: 20
        }
      },
      animationType: 'scale',
      animationEasing: 'elasticOut',
      animationDelay: function (idx) {
        return Math.random() * 200
      }
    }
  ]
}
const dataCatagaryLine = {
  backgroundColor: '#f8f8f9',
  color: color,
  title: {
    text: '网订数据态势',
    top: 10,
    left: 10
  },
  tooltip: {
    trigger: 'axis',
    axisPointer: {
      type: 'cross',
      label: {
        backgroundColor: '#6a7985'
      }
    }
  },
  toolbox: {
    feature: {
      saveAsImage: {}
    }
  },
  grid: {
    // left: '3%',
    // right: '4%',
    bottom: '3%'
    // containLabel: true
  },
  xAxis:
  {
    type: 'category',
    // boundaryGap: false,
    data: []
  },
  yAxis: [
    {
      type: 'value'
    }
  ],
  series: []
}
const dataTicketLine = {
  backgroundColor: '#f8f8f9',
  color: color,
  title: {
    text: '窗口数据态势',
    top: 10,
    left: 10
  },
  tooltip: {
    trigger: 'axis',
    axisPointer: {
      type: 'cross',
      label: {
        backgroundColor: '#6a7985'
      }
    }
  },
  toolbox: {
    feature: {
      saveAsImage: {}
    }
  },
  grid: {
    // left: '3%',
    // right: '4%',
    bottom: '3%'
    // containLabel: true
  },
  xAxis:
  {
    type: 'category',
    // boundaryGap: false,
    data: []
  },
  yAxis: [
    {
      type: 'value'
    }
  ],
  series: []
}
const warnCatagaryLine = {
  backgroundColor: '#f8f8f9',
  color: color,
  title: {
    text: '预警态势',
    top: 10,
    left: 10
  },
  tooltip: {
    trigger: 'axis',
    axisPointer: {
      type: 'cross',
      label: {
        backgroundColor: '#6a7985'
      }
    }
  },
  legend: {
    data: []
  },
  toolbox: {
    feature: {
      saveAsImage: {}
    }
  },
  grid: {
    left: '3%',
    right: '4%',
    bottom: '3%',
    containLabel: true
  },
  xAxis: [
    {
      type: 'category',
      boundaryGap: false,
      data: []
    }
  ],
  yAxis: [
    {
      type: 'value'
    }
  ],
  series: [
    {
      name: '邮件营销',
      type: 'line',
      data: [120, 132, 101, 134, 90, 230, 210]
    },
    {
      name: '联盟广告',
      type: 'line',
      data: [220, 182, 191, 234, 290, 330, 310]
    },
    {
      name: '视频广告',
      type: 'line',
      data: [150, 232, 201, 154, 190, 330, 410]
    },
    {
      name: '直接访问',
      type: 'line',
      data: [320, 332, 301, 334, 390, 330, 320]
    }
  ]
}
const dayWarnBar = {
  backgroundColor: '#f8f8f9',
  color: color,
  title: {
    text: '当日预警',
    top: 10,
    left: 10
  },
  legend: {
    data: ['当日预警'],
    align: 'left'
  },
  toolbox: {
    // y: 'bottom',
    feature: {
      dataView: {},
      saveAsImage: {
        pixelRatio: 2
      }
    }
  },
  tooltip: {},
  xAxis: {
    silent: false,
    splitLine: {
      show: false
    }
  },
  yAxis: {
  },
  series: [{
    name: '重点人员预警',
    type: 'bar',
    animationDelay: function (idx) {
      return idx * 10
    }
  }],
  animationEasing: 'elasticOut',
  animationDelayUpdate: function (idx) {
    return idx * 5
  }
}
export {dataCatagrayPie, warningCatagrayPie, dataCatagaryLine, dayWarnBar, warnCatagaryLine, dataTicketLine}
