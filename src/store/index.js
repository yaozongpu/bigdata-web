import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    permission: null,
    userName: ''
  },
  mutations: {
    setPermission (state, permission) {
      state.permission = permission
      sessionStorage.setItem('permission', permission)
    },
    setUserName (state, userName) {
      state.userName = userName
      sessionStorage.setItem('userName', userName)
    },
    setRealName (state, realName) {
      state.realName = realName
      sessionStorage.setItem('realName', realName)
    }
  },
  getters: {
    permission: state => {
      state.permission = sessionStorage.getItem('permission')
      return state.permission.split(',')
    },
    userName: state => {
      state.userName = sessionStorage.getItem('userName')
      return state.userName
    },
    realName: state => {
      state.realName = sessionStorage.getItem('realName')
      return state.realName
    }
  }
})
