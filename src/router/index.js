import Vue from 'vue'
import Router from 'vue-router'
import main from '@/components/main'
import home from '@/components/home'
import personTicket from '@/components/personTicket'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/main',
      component: main,
      children: [
        {
          path: 'home',
          name: 'home',
          component: home
        },
        {
          path: 'personTicket',
          name: 'personTicket',
          component: personTicket
        }
      ]
    }
  ]
})
