// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import resource from 'vue-resource'
import iView from 'iview'
import 'iview/dist/styles/iview.css'
import moment from './js/moment.js'
import store from './store/index.js'
import permission from './js/permission.js'
import global from './js/global.js'
Vue.use(iView)
Vue.use(resource)
Vue.use(moment)
Vue.use(permission)
Vue.use(global)
Vue.config.productionTip = false
// Vue.http.interceptors.push(function(request) {
//    return function(response) {
//        if(response.status === 405) {}
//    }
// })

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  template: '<App/>',
  components: { App }
})
